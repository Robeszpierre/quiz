package Server.Model;

public class Question {

    private int questionID,
                correctAnswer;

    private String  question,
                    answer1,
                    answer2,
                    answer3,
                    answer4;

    //<editor-fold defaultstate="collapsed" desc="Constructors">

    Question(int questionID, String question, String answer1, String answer2, String answer3, String answer4, int correctAnswer){
        this.questionID=questionID;
        this.question=question;
        this.answer1=answer1;
        this.answer2=answer2;
        this.answer3=answer3;
        this.answer4=answer4;
        this.setCorrectAnswer(correctAnswer);
    }

    public Question(){
        this.questionID=0;
        this.question="";
        this.answer1="";
        this.answer2="";
        this.answer3="";
        this.answer4="";
        this.setCorrectAnswer(0);
    }

    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Getters and Setters">

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer1() {
        return answer1;
    }

    public void setAnswer1(String answer1) {
        this.answer1 = answer1;
    }

    public String getAnswer2() {
        return answer2;
    }

    public void setAnswer2(String answer2) {
        this.answer2 = answer2;
    }

    public String getAnswer3() {
        return answer3;
    }

    public void setAnswer3(String answer3) {
        this.answer3 = answer3;
    }

    public String getAnswer4() {
        return answer4;
    }

    public void setAnswer4(String answer4) {
        this.answer4 = answer4;
    }

    public int getQuestionID() {
        return questionID;
    }

    public void setQuestionID(int questionID) {
        this.questionID = questionID;
    }

    public int getCorrectAnswer() {
        return correctAnswer;
    }

    public void setCorrectAnswer(int correctAnswer) {
        this.correctAnswer = correctAnswer;
    }

    //</editor-fold
}