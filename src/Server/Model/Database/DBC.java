package Server.Model.Database;

import Server.Model.Question;

import java.io.BufferedReader;
import java.io.FileReader;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import static Util.LocalUtil.getRandomNumber;
import static Util.LocalUtil.stringToQuestion;

//DataBaseController
public class DBC extends Config {

    //<editor-fold defaultstate="collapsed" desc="Reading from txt">

    public ArrayList<Question> fileToQuestions(){
        ArrayList<Question> questions = null;
        try {
            System.out.println(System.getProperty("user.dir"));
            BufferedReader br = new BufferedReader( new FileReader("/src/Server/Model/Database/tableSources.txt") );
            questions = new ArrayList<>();
            String line;
            while ((line=br.readLine()) != null && !line.equals("")){
                questions.add(stringToQuestion(line));
            }
        }catch(Exception e){
            System.out.println("Error occurred while reading from files | "+e);
        }
        return  questions;
    }

    public void cardsToDB(ArrayList<Question> cards){
        for (Question card:cards) {
            addQuestion(card);
        }
    }

    //</editor-fold>

    public void addQuestion(Question question){
        String sql = "insert into Question(QUESTION, ANSWER1, ANSWER2, ANSWER3, ANSWER4, CORRECT_ANSWER) values (?,?,?,?,?,?)";
        try {
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setString(1,question.getQuestion());
            preparedStatement.setString(2,question.getAnswer1());
            preparedStatement.setString(3,question.getAnswer2());
            preparedStatement.setString(4,question.getAnswer3());
            preparedStatement.setString(5,question.getAnswer4());
            preparedStatement.setInt(6,question.getCorrectAnswer());
            preparedStatement.execute();
        } catch (SQLException e) {
            System.out.println("Error in addQuestion method | " +e);
        }
    }

    public Question getQuestion(){
        String sql1 = "(select COUNT(*) as rowcount from Question)";
        String sql2 = "Select * from Question where Question_ID=?";
        int numOfQuestions=0;

        ResultSet rs = null;
        try {
            rs = createStatement.executeQuery(sql1);
            rs.next();
            numOfQuestions=rs.getInt("rowcount");
        } catch (SQLException e) {
            e.printStackTrace();
        }

        int randomNumber=getRandomNumber(1,numOfQuestions);

        Question question = null;
        try {
            PreparedStatement preparedStatement = conn.prepareStatement(sql2);
            preparedStatement.setInt(1,randomNumber);
            rs = preparedStatement.executeQuery();
            question = new Question();
            while (rs.next()){
                question.setQuestionID(rs.getInt("Question_ID"));
                question.setQuestion(rs.getString("question"));
                question.setAnswer1(rs.getString("answer1"));
                question.setAnswer2(rs.getString("answer2"));
                question.setAnswer3(rs.getString("answer3"));
                question.setAnswer4(rs.getString("answer4"));
                question.setCorrectAnswer(rs.getInt("correct_answer"));
            }
        } catch (SQLException e) {
            System.out.println("Error in getQuestion method | " +e);
        }
        return question;
    }

}
