package Server.Model.Database;

import java.sql.*;

public class Config {
    final String JDBC_DRIVER = "org.apache.derby.jdbc.EmbeddedDriver";
    final String URL = "jdbc:derby:QuizDB;create=true";
    final String USERNAME = "";
    final String PASSWORD = "";

    //Létrehozzuk a kapcsolatot (hidat)
    Connection conn = null;
    Statement createStatement = null;
    DatabaseMetaData dbmd = null;

    //create Database
    public Config() {
        //Megpróbáljuk életre kelteni
        try {
            conn = DriverManager.getConnection(URL);
            System.out.println("A híd létrejött");
        } catch (SQLException ex) {
            System.out.println("Valami baj van a connection (híd) létrehozásakor.");
            System.out.println("" + ex);
        }

        //Ha életre kelt, csinálunk egy megpakolható teherautót
        if (conn != null) {
            try {
                createStatement = conn.createStatement();
            } catch (SQLException ex) {
                System.out.println("Valami baj van van a createStatament (teherautó) létrehozásakor.");
                System.out.println("" + ex);
            }
        }

        //Megnézzük, hogy üres-e az adatbázis? Megnézzük, létezik-e az adott adattábla.
        try {
            dbmd = conn.getMetaData();
        } catch (SQLException ex) {
            System.out.println("Valami baj van a DatabaseMetaData (adatbázis leírása) létrehozásakor..");
            System.out.println("" + ex);
        }


        //create question table
        try {
            ResultSet rs = dbmd.getTables(null, "APP", "Question", null);
            if(!rs.next())
            {
                createStatement.execute("create table Question(Question_ID INT not null primary key GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1), " +
                        "question varchar(300) not null, answer1 varchar(300) not null, answer2 varchar(300) not null, answer3 varchar(300) not null, answer4 varchar(300) not null, correct_answer int not null)");
                System.out.println("létrehozva question");
            }
        } catch (SQLException ex) {
            System.out.println("Valami baj van a question adattábla létrehozásakor.");
            System.out.println(""+ex);
        }
    }

    public void shutDown(){
        try {
            DriverManager.getConnection("jdbc:derby:;shutdown=true");

        } catch (SQLException ex) {
            if (ex.getSQLState().equals("XJ015")) {
                System.out.println("Derby shutdown normally");
            } else {
                System.out.println("Could not close the Database | " +ex);
            }
        }
    }
}