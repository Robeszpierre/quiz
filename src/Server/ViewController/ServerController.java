package Server.ViewController;

import Server.Controller.DiscoveryThread;
import javafx.fxml.FXML;

public class ServerController {
    @FXML
    private void newRound() {
        DiscoveryThread.newRound();
    }
    @FXML
    private void endRound() {
        DiscoveryThread.endRound();
    }
}
