package Server;

import Server.Controller.DiscoveryThread;
import Server.Model.Database.DBC;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class ServerMain extends Application {

    private static DBC db= null;

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("ServerView/server.fxml"));

        Scene scene = new Scene(root);

        primaryStage.setTitle("Quiz Server");
        primaryStage.setMaximized(true);
        primaryStage.setScene(scene);
        primaryStage.show();
        scene.getStylesheets().add(this.getClass().getResource("/view.css").toExternalForm());
    }

    public static void main(String[] args) {
        db=new DBC();
        Thread discoveryThread = new Thread(DiscoveryThread.getInstance());
        discoveryThread.start();

        launch(args);

        db.shutDown();
        System.exit(0);
    }

    public static DBC getDb() {
        return db;
    }
}