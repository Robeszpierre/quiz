package Util;

import java.util.Random;

import Server.Model.Question;

public class LocalUtil {

    /**
     * Converts an input String of format : QUESTIONx:xANSWER_1x:xANSWER_2x:xANSWER_3x:xANSWER_4x:xCORRECT_ANSWER
     * to a Question object with the parameters above.
     *
     * @param line the input String (in the specified format)
     * @return the Question object derived from the parameter
     */
    public static Question stringToQuestion(String line){
        Question temp = new Question();

        //Melyik gites parancsért öl meg Krisztián?x:xgit initx:xgit add .x:xgit push -f origin masterx:xgit commit -m "Title"x:x3

        temp.setQuestion(line.split("x:x")[0]);
        temp.setAnswer1(line.split("x:x")[1]);
        temp.setAnswer2(line.split("x:x")[2]);
        temp.setAnswer3(line.split("x:x")[3]);
        temp.setAnswer4(line.split("x:x")[4]);
        temp.setCorrectAnswer(Integer.parseInt(line.split("x:x")[5]));

        return temp;
    }

    /**
     * Generates a random integer between the two parameters.
     *
     * @param min the minimum value of the generation
     * @param max the maximum value of the generation
     * @return a random value between the parameters
     */
    public static int getRandomNumber(int min, int max){
        Random r = new Random();
        int randomNumber = r.nextInt(max-min) + min;
        return randomNumber;
    }

}
