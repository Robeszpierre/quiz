package Client.Controller;

import Server.Model.Question;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

import java.io.IOException;
import java.net.*;
import java.util.ArrayList;
import java.util.Enumeration;

import static Util.LocalUtil.stringToQuestion;

public class GameController {

    //<editor-fold defaultstate="collapsed", desc="FXML">

    //<editor-fold defaultstate="collapsed", desc="Start">
    @FXML
    GridPane startPane;

    @FXML
    Label waitNextRoundLabel;
    @FXML
    Button joinToGameButton;
    //</editor-fold>

    //<editor-fold defaultstate="collapsed", desc="Question">
    @FXML
    Pane questionPane;

    @FXML
    Label questionLabel;
    @FXML
    Label pointLabel;
    @FXML
    Circle pointCirc;

    @FXML
    HBox answer1Hbox;
    @FXML
    HBox answer2Hbox;
    @FXML
    HBox answer3Hbox;
    @FXML
    HBox answer4Hbox;
    @FXML
    Label answer1Label;
    @FXML
    Label answer2Label;
    @FXML
    Label answer3Label;
    @FXML
    Label answer4Label;

    @FXML
    Button sendAnswerButton;
    //</editor-fold>

    //</editor-fold>

    private ArrayList<HBox> answers;
    private int goodAnswer=0;
    private Question actualQuestion=new Question();
    private int selectedAnswer=0;
    private boolean isAnswerChosen=false;
    private boolean questionExistence=true;

    //<editor-fold defaultstate="collapsed" desc="Switching panels">

    @FXML
    private void registerToNextRound(){
        questionExistence=true;
        getNextQuestion();

        if (questionExistence){
            waitNextRoundLabel.setVisible(false);
            startPane.setVisible(false);
            questionPane.setVisible(true);
            makeAnswerArray();
        }
        else {
            waitNextRoundLabel.setVisible(true);
        }
    }

        private void makeAnswerArray() {
        answers=new ArrayList();
        answers.add(answer1Hbox);
        answers.add(answer2Hbox);
        answers.add(answer3Hbox);
        answers.add(answer4Hbox);
    }

    @FXML
    private void backToStart(){
        questionPane.setVisible(false);
        startPane.setVisible(true);
    }

    //</editor-fold>

    @FXML
    private  void alert(String s){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle(s);
        alert.setHeaderText("");
        alert.setContentText(s);
        alert.show();
    }

    //<editor-fold defaultstate="collapsed" desc="Highlighting answers">

    @FXML
    private void answerClicked(MouseEvent mouseEvent) {
        HBox selectedAnswer = (HBox) mouseEvent.getSource();
        ObservableList<String> a = selectedAnswer.getStyleClass();
        for(String b: a){
            if(b.equals("highlightAnswer")){
                isAnswerChosen=false;
            }else{
                isAnswerChosen=true;
            }
        }

        if(isAnswerChosen){
            sendAnswerButton.setVisible(true);
        }else{
            sendAnswerButton.setVisible(false);
        }
        String id = selectedAnswer.getId();

        int selectedAnswerNumber=1;
        for(HBox selected: answers ){
            if(selected.getId().equals(id) && !selected.getStyleClass().contains("highlightAnswer")){
                selected.getStyleClass().add("highlightAnswer");
                this.selectedAnswer = selectedAnswerNumber;
            }else{
                selected.getStyleClass().remove("highlightAnswer");
            }
            if (isAnswerChosen) {
                sendAnswerButton.setVisible(true);
            } else {
                sendAnswerButton.setVisible(false);
            }
            selectedAnswerNumber++;
        }
    }

    private void removeSelection(){
        for(HBox selected: answers ){
            selected.getStyleClass().remove("highlightAnswer");
        }
        isAnswerChosen=false;
        sendAnswerButton.setVisible(false);
    }

    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Loading Questions">

    private void getNextQuestion(){
        // Find the server using UDP broadcast
        try {
            //Open a random port to send the package
            DatagramSocket c = new DatagramSocket();
            c.setBroadcast(true);

            byte[] sendData = "DISCOVER_FUIFSERVER_REQUEST".getBytes();

            if (goodAnswer == 5) {
                sendData = "END".getBytes();
            }

            // Broadcast the message over all the network interfaces
            Enumeration interfaces = NetworkInterface.getNetworkInterfaces();
            while (interfaces.hasMoreElements()) {
                NetworkInterface networkInterface = (NetworkInterface) interfaces.nextElement();

                if (networkInterface.isLoopback() || !networkInterface.isUp()) {
                    continue; // Don't want to broadcast to the loopback interface
                }

                for (InterfaceAddress interfaceAddress : networkInterface.getInterfaceAddresses()) {
                    InetAddress broadcast = interfaceAddress.getBroadcast();
                    if (broadcast == null) {
                        continue;
                    }

                    // Send the broadcast package!
                    try {
                        DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, broadcast, 8888);
                        c.send(sendPacket);
                    } catch (Exception e) {
                    }

                    System.out.println(getClass().getName() + ">>> Request packet sent to: " + broadcast.getHostAddress() + "; Interface: " + networkInterface.getDisplayName());
                }
            }

            System.out.println(getClass().getName() + ">>> Done looping over all network interfaces. Now waiting for a reply!");

            //Wait for a response
            byte[] recvBuf = new byte[15000];
            DatagramPacket receivePacket = new DatagramPacket(recvBuf, recvBuf.length);
            c.receive(receivePacket);

            //We have a response
            System.out.println(getClass().getName() + ">>> Broadcast response from server: " + receivePacket.getAddress().getHostAddress());

            //Check if the message is correct
            String message = new String(receivePacket.getData(), "UTF-8").trim();

            translateMessage(message);

            //Close the port!
            c.close();
        } catch (IOException ex) {
            System.out.println(ex);
        }
    }

        private void translateMessage(String message){
            switch(message){
                case "NOROUND":
                    questionExistence=false;
                    if (!startPane.isVisible()==true){
                        alert("Veszítettél!");
                        backToStart();
                    }
                    goodAnswer=0;
                    break;
                case "WINNER":
                    alert("Győztél!");
                    goodAnswer=0;
                    backToStart();
                    break;
                default:
                    if (message.startsWith("NEWROUND")){
                        goodAnswer=0;
                        message=message.substring(8);
                    }
                    System.out.println(message);
                    Question q = stringToQuestion(message);
                    actualQuestion = q;

                    questionLabel.setText(q.getQuestion());
                    answer1Label.setText(q.getAnswer1());
                    answer2Label.setText(q.getAnswer2());
                    answer3Label.setText(q.getAnswer3());
                    answer4Label.setText(q.getAnswer4());
                    break;
            }
        }

    @FXML
    private void sendAnswer() {
        checkAnswer();
        getNextQuestion();
        removeSelection();
    }

    private void checkAnswer() {
        if(actualQuestion.getCorrectAnswer()==selectedAnswer){
            goodAnswer++;
            pointCirc.setFill(Color.LIGHTGREEN);
        }else{
            goodAnswer=0;
            pointCirc.setFill(Color.LIGHTCORAL);
        }
        pointLabel.setText(""+goodAnswer);
    }

    //</editor-fold>

}