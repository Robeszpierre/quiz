package Client.Controller;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class ClientMain extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("/Game.fxml"));

        Scene scene = new Scene(root);

        primaryStage.setTitle("Quiz Client");
        primaryStage.setWidth(900);
        primaryStage.setHeight(500);
        primaryStage.setScene(scene);
        primaryStage.show();
        scene.getStylesheets().add(this.getClass().getResource("/view.css").toExternalForm());
    }

    public static void main(String[] args) {
        launch(args);
    }
}